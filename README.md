# Oven #

Sample web socket server and client for smart oven project

### Purpose ###

* Client will sent temperature of oven to server every second
* Server will log temp data to file

### Setup ###

* Clone this repo
* Install golang
* go get all dependencies
* go build

2016 by Gampol T.