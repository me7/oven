package main

import (
	"flag"
	"io"
	"log"
	"net/http"
)

var addr = flag.String("addr", "localhost:8080", "http server address")

func main() {
	flag.Parse()
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

//sample home function
func home(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Hello World")
}
